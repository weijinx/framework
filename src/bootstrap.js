const Stage = require('telegraf/stage');
const { match } = require('telegraf-i18n');

/**
 * Bootstrap class
 */
class Bootstrap {
	static load (type, ...args) {
		switch (type) {
		case 'config':
			return Object.assign({}, args[0], args[1]);
		case 'stages':
			return new Stage(args[0].concat(args[1]));
		case 'hears':
			const hears = Object.assign({}, args[0], args[1]);
			const hearsKeys = Object.keys(hears);

			if (hearsKeys.length > 0) {
				Promise.all(hearsKeys).then(keys => {
					return keys.map(key => {
						let validKey = typeof key === 'function' ? key(args[2]) : key;
						return args[3].hears(match(validKey), hears[validKey]);
					});
				});
			}
			break;
		case 'actions':
			const actions = args[0].concat(args[1]);

			if (actions.length > 0) {
				return Promise.all(actions).then(actions => {
					return actions.map(action => {
						return args[2].action(action[0], action[1]);
					});
				});
			}
			break;
		}
	}
}

module.exports = Bootstrap;
