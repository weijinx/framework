const api = require('../client')

module.exports = async (uri, pk) => {
	try {
		const res = await api.delete(uri + '/' + pk)
		return res.data
	} catch (e) {
		console.log(e)
	}
}
