const api = require('../client')

module.exports = async (uri, pk, data = {}) => {
	try {
		const res = await api.put(uri + '/' + pk, data)
		return res.data
	} catch (e) {
		throw e
	}
}
