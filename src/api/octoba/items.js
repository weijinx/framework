const api = require('../client')

module.exports = async (uri, params = {}) => {
	try {
		// const total = params.total
		delete params.total
		const res = await api.get(uri, {params: params})
		return res.data
	} catch (e) {
		console.log(e)
	}
}
