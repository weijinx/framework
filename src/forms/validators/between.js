const validator = require('validator')

module.exports = (rule, value, i18n) => {
	const lengths = rule.split(':')[1]
	const min = parseInt(lengths.split(',')[0], 10)
	const max = parseInt(lengths.split(',')[1], 10)
	const valid = validator.isByteLength(value, {min: min, max: max})

	if (!valid) {
		return i18n.t('octoba_validation_max', {from: min, to: max})
	}
}
