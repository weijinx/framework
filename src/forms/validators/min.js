const validator = require('validator')

module.exports = (rule, value, i18n) => {
	const length = parseInt(rule.split(':')[1], 10)
	const valid = validator.isByteLength(value, {min: length})

	if (!valid) {
		return i18n.t('octoba_validation_min', {num: length})
	}
}
