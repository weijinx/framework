const validator = require('validator')

module.exports = (rule, value, i18n) => {
	const length = parseInt(rule.split(':')[1], 10)
	const valid = validator.isByteLength(value, {min: 1, max: length})

	if (!valid) {
		return i18n.t('octoba_validation_max', {num: length})
	}
}
