const Telegraf = require('telegraf');
const session = require('telegraf/session');
const config = require('./config');
const I18n = require('./utils/i18n');
const path = require('path');
const apiClient = require('./api/client');
const { defaultInlineGoto, validate } = require('./utils/helpers');
const Bootstrap = require('./bootstrap');
const itemsWithPagination = require('./handlers/items');

class App {
	constructor (params) {
		// Config
		this.config = Bootstrap.load('config', config, params);
		// i18n (translations)
		this.i18n = new I18n({
			defaultLanguage: this.config.lang,
			allowMissing: true,
			sessionName: 'session',
			useSession: true,
			directory: path.resolve(__dirname, 'locales')
		}, params.langsFolder);
		// Bot
		this.bot = new Telegraf(this.config.bot_token);
		// API Client
		this.api = apiClient;
	}

	getModel (name) {
		try {
			return this.config.scenarios.models[name];
		} catch (e) {
			console.log(`Model "${name}" not found. Exception: `, e);
		}
	}

	getKeyboard (name, params = {}) {
		try {
			const keyboardFunc = this.config.scenarios.keyboards[name];
			const isFunc = typeof keyboardFunc === 'function';

			if (!isFunc && name === 'resourceGoto') {
				return defaultInlineGoto(this, params);
			}

			return isFunc ? keyboardFunc(this, params) : false;
		} catch (e) {
			console.log('exception:!!!!', e);
		}
	}

	/**
	 * Init bot function.
	 *
	 * @param
	 */
	async initBot () {
		const bot = this.bot;
		const ctx = bot.context;

		ctx.config = this.config;
		ctx.api = {
			client: this.api,
			octoba: {
				getItems: require('./api/octoba/items'),
				getItem: require('./api/octoba/item'),
				storeItem: require('./api/octoba/store'),
				updateItem: require('./api/octoba/update'),
				updateOne: require('./api/octoba/update-one'),
				deleteItem: require('./api/octoba/delete')
			}
		};
		ctx.getModel = this.getModel;
		ctx.validate = validate;
		ctx.getKeyboard = this.getKeyboard;
		ctx.itemsWithPagination = itemsWithPagination;

		const stages = await Bootstrap.load('stages', this.config.octobaScenes, this.config.scenes);

		// Bots middleware
		bot.use(session());
		bot.use(this.i18n.middleware());
		bot.use(stages.middleware());

		// Init commands
		bot.start(this.config.commands.start);
		bot.help(this.config.commands.help);
		bot.action('octoba:main', this.config.commands.start);
		bot.hears(
			this.i18n.t(this.config.lang, 'octoba_main'),
			this.config.commands.start
		);

		Bootstrap.load('hears', this.config.octobaHears, this.config.hears, this.i18n, bot);
		Bootstrap.load('actions', this.config.octobaActions, this.config.actions, bot);

		// Catching errors from bot
		bot.catch(err => {
			console.log('Bot error: ', err);
		});

		if (process.env.NODE_ENV === 'local') {
			console.log('Starting polling...');
			bot.startPolling();
		}
	}
}

module.exports = App;
