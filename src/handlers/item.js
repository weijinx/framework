const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

module.exports = async ctx => {
	let modelName = ctx.match[1];
	const pk = parseInt(ctx.match[2], 10);
	let model = ctx.getModel(modelName);

	try {
		const data = await ctx.api.octoba.getItem(model.crud.item.uri, pk);
		let item = data.item !== undefined ? data.item : data.data;
		let keyboard = [];

		body =
			`${ctx.i18n.t('octoba_model_item', {
				modelName: model.name,
				name: item[model.crud.item.title_attribute]
			})  }\n\n`;

		keyboard.push([
			Markup.callbackButton(
				ctx.i18n.t('octoba_model_update_all'),
				`octoba.item.update:${modelName},${item[model.pk]}`
			)
		]);

		let render_attributes = model.crud.item.render_attributes;
		if (render_attributes.length > 0) {
			render_attributes.map(attribute => {
				body += `*${model.labels[attribute]}:* ${item[attribute]}\n`;
			});
		}

		let render_update_attributes = model.crud.item.render_update_attributes;
		if (render_update_attributes.length > 0) {
			keyboard[1] = [];
			render_update_attributes.map(attribute => {
				keyboard[1].push(
					Markup.callbackButton(
						ctx.i18n.t('octoba_model_update_attribute', {
							label: model.labels[attribute].toLowerCase()
						}),
						`octoba.item.update_one:${modelName},${item[model.pk]},${attribute}`
					)
				);
			});
		}

		// Delete record button
		keyboard.push([
			Markup.callbackButton(
				ctx.i18n.t('octoba_model_delete_button'),
				`octoba.item.delete:${modelName},${item[model.pk]}`
			)
		]);

		// Go to main menu button
		keyboard.push([
			Markup.callbackButton(
				ctx.i18n.t('octoba_model_goto_list'),
				`octoba.pager:${modelName},1`
			),
			Markup.callbackButton(
				ctx.i18n.t('octoba_model_goto_main'),
				'octoba:main'
			)
		]);

		return ctx.editMessageText(
			body,
			Extra.markdown().markup(m => {
				return m.inlineKeyboard(keyboard);
			})
		);
	} catch (e) {
		throw e;
	}
};
