module.exports = ctx => {
	let modelName = ctx.match[1];
	let pk = parseInt(ctx.match[2], 10);
	let model = ctx.getModel(modelName);

	return ctx.scene.enter('octoba-form-many', {
		form: {
			uri: model.crud.update.uri,
			prepare: false,
			validators: model.crud.update.form.validators,
			attributes: model.crud.update.form.attributes,
			labels: model.labels,
			type: 'update',
			id: pk,
			modelName: modelName
		},
		data: {}
	});
};
