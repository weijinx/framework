const Extra = require('telegraf/extra');
const { simple } = require('../utils/pagination');

module.exports = async (ctx, modelName, params = {}, edited = false) => {
	let model = ctx.getModel(modelName);
	let uri = model.crud.query.uri;

	try {
		const paginator = await ctx.api.octoba.getItems(uri, params);
		const keyboardItems = simple(paginator, {
			modelName: modelName,
			label_attribute: model.crud.query.label_attribute,
			pk: model.pk,
			i18n: ctx.i18n
		});

		if (edited) {
			return ctx.editMessageText(
				ctx.i18n.t('octoba_messages_loaded'),
				Extra.markdown().markup(m => {
					return m.inlineKeyboard(keyboardItems);
				})
			);
		} else {
			return ctx.replyWithMarkdown(
				ctx.i18n.t('octoba_messages_loaded'),
				Extra.markdown().markup(m => {
					return m.inlineKeyboard(keyboardItems);
				})
			);
		}
	} catch (e) {
		throw e;
	}
};
