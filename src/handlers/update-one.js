module.exports = ctx => {
	let modelName = ctx.match[1];
	let pk = parseInt(ctx.match[2], 10);
	let attribute = ctx.match[3];
	let model = ctx.getModel(modelName);

	return ctx.scene.enter('octoba-form-one', {
		form: {
			uri: model.crud.update_one.uri,
			prepare: false,
			validators: model.crud.update.form.validators,
			attribute: attribute,
			labels: model.labels,
			type: 'updateOne',
			id: pk,
			modelName: modelName
		},
		data: {}
	});
};
