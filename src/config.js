module.exports = {
	lang: 'ru',
	timezone: 'framework\'s timezone',
	formatDate: 'dates: YYYY-MM-DD HH:mm:ss',
	octobaMiddleware: [],
	octobaScenes: [
		require('./scenes/formOne'),
		require('./scenes/formMany'),
		require('./scenes/delete')
	],
	octobaHears: {
		octoba_help: require('../app/handlers/help')
	},
	octobaActions: [
		[/octoba.pager:(.*),(\d+)/i, require('./handlers/items-paginate')],
		[/octoba.item:(.*),(.*)/i, require('./handlers/item')],
		[/octoba.item.update:(.*),(.*)/i, require('./handlers/update')],
		[/octoba.item.update_one:(.*),(.*),(.*)/i, require('./handlers/update-one')],
		[/octoba.item.delete:(.*),(.*)/i, require('./handlers/delete')]
	],
	octobaValidators: {
		min: require('./forms/validators/min'),
		max: require('./forms/validators/max'),
		between: require('./forms/validators/between')
	}
}
