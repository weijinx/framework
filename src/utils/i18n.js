const TelegrafI18n = require("telegraf-i18n/lib/i18n");

class I18n extends TelegrafI18n {
  constructor(config, projectDirectory) {
    super(config);
    this.projectDirectory = projectDirectory;
    this.loadLocales(this.projectDirectory);
  }
}

module.exports = I18n;
